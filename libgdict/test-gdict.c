


#include <gnome.h>
#include "gdict-defbox.h"

GtkWidget *entry;

static void
app_close_cb (GtkWidget *window, GdkEventAny *ev, gpointer data)
{

	gtk_main_quit ();
	
}

static void
lookup_button_cb (GtkButton *button, gpointer data)
{
	GtkWidget *defbox = data;
	gchar *text;
 
 	text = gtk_editable_get_chars (GTK_EDITABLE (entry), 0, -1);
 	
 	gdict_defbox_lookup (GDICT_DEFBOX (defbox), text);
 	
 	g_free (text);
	
}

static void
entry_activate_cb (GtkEditable *editable, gpointer data)
{
	GtkWidget *defbox = data;
	gchar *text;
 
 	text = gtk_editable_get_chars (editable, 0, -1);
 	
 	gdict_defbox_lookup (GDICT_DEFBOX (defbox), text);
 	
 	g_free (text);
 	
}

int 
main (int argc, char **argv)
{

	GtkWidget *app;
	GtkWidget *vbox;
	GtkWidget *hbox;
	GtkWidget *button;
	GtkWidget *scrolled;
	GtkWidget *defbox;
	
	gnome_init ("test-gdict", "0.1", argc, argv);
	
	
	app = gnome_app_new ("test-gdict", "GDict Plugin Test");
	gtk_window_set_default_size (GTK_WINDOW (app), 420, 300);
	gtk_widget_show (app);
	gtk_signal_connect (GTK_OBJECT (app), "delete_event",
		            GTK_SIGNAL_FUNC (app_close_cb), NULL);
	
	vbox = gtk_vbox_new (FALSE, GNOME_PAD_SMALL);
	gtk_widget_show (vbox);
	gnome_app_set_contents (GNOME_APP (app), vbox);
	
	hbox = gtk_hbox_new (FALSE, GNOME_PAD_SMALL);	
	gtk_container_set_border_width (GTK_CONTAINER (hbox), GNOME_PAD_SMALL);
	gtk_widget_show (hbox);
	gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 0);
	
	button = gtk_button_new_with_label ("Look Up");
	gtk_widget_show (button);
	gtk_box_pack_start (GTK_BOX (hbox), button, FALSE, FALSE, 0);
	
	entry = gtk_entry_new ();
	gtk_widget_show (entry);
	gtk_box_pack_start (GTK_BOX (hbox), entry, TRUE, TRUE, 0);
	
	scrolled = gtk_scrolled_window_new (NULL, NULL);
	gtk_container_set_border_width (GTK_CONTAINER (scrolled), GNOME_PAD_SMALL); 
    	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled),
                                        GTK_POLICY_AUTOMATIC,
                                        GTK_POLICY_AUTOMATIC);
        gtk_widget_show (scrolled);
        gtk_box_pack_start (GTK_BOX (vbox), scrolled, TRUE, TRUE, 0);
	
	defbox = gdict_defbox_new ();
	gtk_widget_show (defbox);
	gtk_container_add (GTK_CONTAINER (scrolled), defbox);
	
	gtk_signal_connect (GTK_OBJECT (button), "clicked",
		            GTK_SIGNAL_FUNC (lookup_button_cb), defbox);
	gtk_signal_connect (GTK_OBJECT (entry), "activate",
			    GTK_SIGNAL_FUNC (entry_activate_cb), defbox);
		
	gtk_main ();
	
}

