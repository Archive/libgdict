




#include <gnome.h>
#include "dict.h"
#include "gdict-defbox.h"

enum {
    HEADWORD,              /* Headword */
    NUMBER,                /* Sub-definition number */
    PRONUNCIATION,         /* Pronunciation */
    ETYMOLOGY,             /* Etymology */
    PART,                  /* Part of speech */
    EXAMPLE,               /* Example phrase */
    BODY,                  /* Definition body */
    XREF,                  /* Cross-reference */
    NUM_TYPES
};

typedef struct _GDictTypeface {
    GdkFont   *font;
    GdkColor  *color;
} GDictTypeface;

struct _GDictDefboxPrivate {

    dict_context_t *context;
    dict_command_t *def_cmd;
    gchar          *database;
    gchar	   *server;
    gint	   port;
    gboolean	   smart;
    gchar         *dfl_strat;
    GDictTypeface  typefaces[NUM_TYPES];
};

void		gdict_pref_load (GDictDefbox *defbox);
