
#include "gdict-defbox-private.h"

typedef struct _TypefaceDefault {
    gchar *font_key;
    gchar *color_key;
    gchar *font;
    GdkColor color;
} TypefaceDefault;

TypefaceDefault defaults[NUM_TYPES] = {
    { "fheadword", "cheadword", "-adobe-helvetica-bold-r-normal-*-*-120-*-*-p-*-iso8859-1", { 45568, 8704, 8704 } },
    { "fnumber", "cnumber", "-adobe-courier-bold-r-normal-*-*-130-*-*-m-*-iso8859-1", { 0, 35584, 35584 } },
    { "fpronunciation", "cpronunciation", "-adobe-times-medium-i-normal-*-*-120-*-*-p-*-iso8859-1", { 7168, 34304, 60928 } },
    { "fetymology", "cetymology", "-adobe-times-medium-i-normal-*-*-120-*-*-p-*-iso8859-1", { 15360, 45824, 28928 } },
    { "fpart", "cpart", "-adobe-times-bold-r-normal-*-*-120-*-*-p-*-iso8859-1", { 30464, 34816, 39168 } },
    { "fexample", "cexample", "-adobe-helvetica-medium-o-normal-*-*-120-*-*-p-*-iso8859-1", { 27392, 36352, 8960 } },
    { "fxref", "cxref", "-adobe-helvetica-medium-r-normal-*-*-120-*-*-p-*-iso8859-1", { 20992, 35584, 35584 } },
    { "fbody", "cbody", "-adobe-times-medium-r-normal-*-*-120-*-*-p-*-iso8859-1", { 0, 0, 0 } }
};

static GdkFont *
config_get_font (gchar *path, gchar *dflt) {
    gchar *key = g_strconcat(path, "=", dflt, NULL);
    gchar *fn_name = gnome_config_get_string(key);
    GdkFont *font = gdk_font_load(fn_name);
    
    g_free(key);
    g_free(fn_name);
    return font;
}

static GdkColor *
config_get_color (GDictDefbox *defbox, gchar *path, 
		  gint red_dflt, gint green_dflt, 
		  gint blue_dflt) {    
    gchar *red_key = g_strdup_printf("%s_red=%d", path, red_dflt);
    gchar *green_key = g_strdup_printf("%s_green=%d", path, green_dflt);
    gchar *blue_key = g_strdup_printf("%s_blue=%d", path, blue_dflt);
    gint red = gnome_config_get_int(red_key);
    gint green = gnome_config_get_int(green_key);
    gint blue = gnome_config_get_int(blue_key);
    GdkColor *color = g_malloc(sizeof(GdkColor));
   
    color->red = red;
    color->green = green;
    color->blue = blue;
    gdk_color_alloc (gtk_widget_get_colormap(GTK_WIDGET (defbox)), color);
    g_free(red_key);
    g_free(green_key);
    g_free(blue_key);
    return color;
}

void
gdict_pref_load (GDictDefbox *defbox) {
    gchar *prefix;
    gint i;

    prefix = g_strdup ("/gdict/Preferences/");
   
    gnome_config_push_prefix(prefix);
    defbox->priv->server = gnome_config_get_string("server=dict.org");
    defbox->priv->port = gnome_config_get_int("port=2628");
    defbox->priv->smart = gnome_config_get_bool("smart=TRUE");

    defbox->priv->database = gnome_config_get_string ("database=!");
    defbox->priv->dfl_strat = gnome_config_get_string ("strategy=lev");

    for (i = 0; i < NUM_TYPES; i++) {
        defbox->priv->typefaces[i].font =
            config_get_font (defaults[i].font_key, defaults[i].font);
        defbox->priv->typefaces[i].color =
            config_get_color (defbox, defaults[i].color_key, defaults[i].color.red,
                              defaults[i].color.green, defaults[i].color.blue);
    }

    gnome_config_pop_prefix();
    g_free(prefix);
    
}


   
   
        